# Demo VR

A-Frame demonstration using a YAML configuration file as source to generate a multiple choice questions based educational activity. A working demo is available at https://feualg.pt/others/demo

## Installation
Just place all files in this repo into a SSL capable Webserver.

## Usage
Edit the config.yaml file to include whatever questions and options you want to see in the demo. Be mindful to correctly chose the question type. `singular = True` means that only one of the options is correct. `singular = False` means it's a multiple choice question.

## Project status
Developed as a Proof of Concept to demonstrate the possibility of configuring VR Web content 'just-in-time' through a simple configuration file.
